lazy val root = (project in file(".")).
  settings(
    name := "com-emde-spark",
    version := "1.0",
    scalaVersion := "2.10.4",
    mainClass in Compile := Some("com.emde.spark.file.FileWordCount")        
  )

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "1.6.1"
libraryDependencies += "org.apache.spark" % "spark-mllib_2.10" % "1.6.1"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

libraryDependencies += "org.postgresql" % "postgresql" % "9.3-1104-jdbc41"

libraryDependencies += "commons-dbcp" % "commons-dbcp" % "1.4"
libraryDependencies += "commons-pool" % "commons-pool" % "1.6"

libraryDependencies += "joda-time" % "joda-time" % "2.9.4"

libraryDependencies ++= Seq(
  "org.scalikejdbc" %% "scalikejdbc"         % "2.4.2",
  "org.scalikejdbc" %% "scalikejdbc-config"  % "2.4.2",
  "com.h2database"  %  "h2"                  % "1.4.192",
  "ch.qos.logback"  %  "logback-classic"     % "1.1.7"
)

// META-INF discarding
mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
   {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
   }
}