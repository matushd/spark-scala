package com.emde.repository

import scalikejdbc._
import scalikejdbc.config._
import org.joda.time._
import com.emde.model.Recommended

object RecommendedRepository {
  
   def create(userId: String, movieId: Integer, prediction: Double)(implicit s: DBSession = AutoSession): Recommended = {
    val date: DateTime = DateTime.now()
    val column = Recommended.column
    val id = sql"insert into ${Recommended.table} (${column.userId}, ${column.movieId}, ${column.prediction}, ${column.date}) values (${userId}, ${movieId}, ${prediction}, ${date})"
      .updateAndReturnGeneratedKey.apply().toInt
    Recommended(id, userId, movieId, prediction, date)
  }
  
  def findSimilar(movieId: Integer)(implicit s: DBSession = AutoSession): List[Int] = {
    sql"SELECT movie_similar_id as movieId FROM movie_similar WHERE movie_id = ${movieId} UNION SELECT movie_id as movieId FROM movie_similar WHERE movie_similar_id = ${movieId}"
    .map(rs => rs.int("movieId")).list.apply()
  }
  
  def findPopular(limit: Integer)(implicit s: DBSession = AutoSession): List[Int] = {
    sql"SELECT movie_id as movieId FROM movie ORDER BY movie_vote_count DESC LIMIT ${limit}"
    .map(rs => rs.int("movieId")).list.apply()
  }
  
  def findBudget(limit: Integer)(implicit s: DBSession = AutoSession): List[Int] = {
    sql"SELECT movie_id as movieId FROM movie ORDER BY movie_budget DESC LIMIT ${limit}"
    .map(rs => rs.int("movieId")).list.apply()
  }
  
  def findRevenue(limit: Integer)(implicit s: DBSession = AutoSession): List[Int] = {
    sql"SELECT movie_id as movieId FROM movie ORDER BY movie_revenue DESC LIMIT ${limit}"
    .map(rs => rs.int("movieId")).list.apply()
  }
}