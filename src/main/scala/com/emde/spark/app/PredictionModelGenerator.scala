package com.emde.spark.app

import org.apache.spark.{SparkConf, SparkContext}
import com.emde.repository.RecommendedRepository
import scalikejdbc.config.DBs
import com.emde.recommend.Service
import org.apache.commons.io.FileUtils
import java.io.File

/**
 * Usage:
 * sbt assembly && /usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.PredictionModelGenerator --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar /home/emde/BigData/ml-latest-small /home/emde/BigData/ml-latest-small/movie 680 16 4 100 0.7
 */
object PredictionModelGenerator {
  
  def main(args: Array[String]): Unit = {
    if (args.length < 7) {
      System.err.println(s"""
        |Usage: PredictionModelGenerator <dataDir> <outputDir> <movieIds>
        |  <dataDir> MovieLens trainee data
        |  <outputDir> generated prediction models directory
        |  <movieIds> movie ids to generate prediction
        |  <numTrees> number of decision trees
        |  <maxDepth> max decision tree depth
        |  <maxBins> max bins
        |  <trainingCount> training data percent
        |
        """.stripMargin)
      System.exit(1)
    }
    
    val viewsFile = args(0) + "/ratings.csv"
    val linksFile = args(0) + "/links.csv"
    val movieIds = args(2).split(",")
    val numTrees = args(3).toInt
    val maxDepth = args(4).toInt
    val maxBins = args(5).toInt
    val trainingCount = args(6).toFloat
    
    // Create the context
    val sparkConf = new SparkConf().setAppName("PredictionModelGenerator")
    val ssc = new SparkContext(sparkConf)
    
    DBs.setupAll()
    
    // ( tmdbid, simiarIds )
    val similars = ssc.parallelize(movieIds.toSeq).map { x => (x.toInt, RecommendedRepository.findSimilar(x.toInt) ) }
      .map( x => (x._1, x._2 ++ RecommendedRepository.findPopular(5)
          ++ x._2.map(RecommendedRepository.findSimilar(_)).reduce((x,y) => x ++ y))).collect()
      
    // movieId, imdbId, tmdbId
    val movieIdMap = ssc.textFile(linksFile)
      .filter(_.nonEmpty)
      .mapPartitionsWithIndex { (idx, iter) => if (idx == 0) iter.drop(1) else iter }
      .map { x => x.split(",") }
      .filter { x => x.size == 3 }
      .map { x => (x(0).toInt, x(2).toInt) }
    // ( movieId, tmdbid )
      
    // userId, movieId, rating, timestamp
    val views = ssc.textFile(viewsFile)
      .filter(_.nonEmpty)
      .mapPartitionsWithIndex { (idx, iter) => if (idx == 0) iter.drop(1) else iter }
      .map { x => x.split(",") }
      .filter { x => x.size == 4 }
      .map { x => ( x(1).toInt, (x(2).toDouble, x(0).toInt) ) }
    // ( movieid, (rating, userId) )
      
    similars.foreach(similar => {
      val path = args(1).concat(similar._1.toString());
      FileUtils.deleteDirectory(new File(path));
      Service.prepareTraineeData(path, similar, movieIdMap, views)
      Service.generatePredictionModel(ssc, path, numTrees, maxDepth, maxBins, trainingCount)
    })
    
    DBs.closeAll()  
  }
}