package com.emde.spark.app

import org.apache.spark.{SparkConf, SparkContext}
import com.emde.recommend.Service
import scalikejdbc.config.DBs
import com.emde.repository.RecommendedRepository
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.kafka.{KafkaUtils, OffsetRange}

/**
 * Usage:
 * sbt assembly && /usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.GenerateMovieRecommendation --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar /home/emde/BigData/ml-latest/predctions/movie 680 localhost:9092 events
 */
object KafkaGenerateMovieRecommendation {
  
  def main(args: Array[String]): Unit = {
    if (args.length < 4) {
      System.err.println(s"""
        |Usage: KafkaGenerateMovieRecommendation <predictionModelsDir> <movieIds> <brokers> <topics>
        |  <predictionModelsDir> generated prediction models directory
        |  <movieIds> movie ids to generate prediction
        |  <brokers> is a list of one or more Kafka brokers
        |  <topics> is a list of one or more kafka topics to consume from
        |
        """.stripMargin)
      System.exit(1)
    }
    
    val movieIds = args(1).split(",")
    val brokers = args(2)
    val topics = args(3)
    
    // Create the context
    val sparkConf = new SparkConf().setAppName("KafkaGenerateMovieRecommendation")
    val ssc = new SparkContext(sparkConf)
    
    
    // Get last 0.5 M messages from Kafka topics
    val offsetRanges = topics.split(",").map { x => OffsetRange(x, 0, 0, 500000) }.toArray
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)
    val messages = KafkaUtils.createRDD[String, String, StringDecoder, StringDecoder](
      ssc, kafkaParams, offsetRanges)
    
    DBs.setupAll()
    
    val lines = messages.map(_._2)
    
    // ( tmdbid, simiarIds )
    val similars = ssc.parallelize(movieIds.toSeq).map { x => (x.toInt, RecommendedRepository.findSimilar(x.toInt) ) }
      .map( x => (x._1, x._2 ++ RecommendedRepository.findPopular(5)
          ++ x._2.map(RecommendedRepository.findSimilar(_)).reduce((x,y) => x ++ y))).collect()
    
    // ( userId, ( movieId, views ) )
    val rating = Service.prepareRating(lines, ssc)
    
    similars.foreach(similar => {
      val path = args(0).concat(similar._1.toString());
      Service.predictRecommend(path, rating, similar, ssc)
    })
    
    DBs.closeAll() 
  }
}