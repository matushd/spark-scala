package com.emde.spark.app

import org.apache.spark.{SparkConf, SparkContext}
import com.emde.recommend.Service
import scalikejdbc.config.DBs
import com.emde.repository.RecommendedRepository
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.kafka.{KafkaUtils, OffsetRange}

/**
 * Usage:
 * sbt assembly && /usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.LogGenerateMovieRecommendation --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar /home/emde/BigData/ml-latest/predctions/movie 680 /home/emde/logs/event.log
 */
object LogGenerateMovieRecommendation {
  
  def main(args: Array[String]): Unit = {
    if (args.length < 3) {
      System.err.println(s"""
        |Usage: LogGenerateMovieRecommendation <predictionModelsDir> <movieIds> <file>
        |  <predictionModelsDir> generated prediction models directory
        |  <movieIds> movie ids to generate prediction
        |  <file> input data file
        |
        """.stripMargin)
      System.exit(1)
    }
    
    // Create the context
    val sparkConf = new SparkConf().setAppName("LogGenerateMovieRecommendation")
    val ssc = new SparkContext(sparkConf)
    
    DBs.setupAll()
    
    val lines = ssc.textFile(args(2))
    val movieIds = args(1).split(",")
    
    // ( tmdbid, simiarIds )
    val similars = ssc.parallelize(movieIds.toSeq).map { x => (x.toInt, RecommendedRepository.findSimilar(x.toInt) ) }
      .map( x => (x._1, x._2 ++ RecommendedRepository.findPopular(5)
          ++ x._2.map(RecommendedRepository.findSimilar(_)).reduce((x,y) => x ++ y))).collect()
    
    // ( userId, ( movieId, views ) )
    val rating = Service.prepareRating(lines, ssc)
    
    similars.foreach(similar => {
      val path = args(0).concat(similar._1.toString());
      Service.predictRecommend(path, rating, similar, ssc)
    })
    
    DBs.closeAll() 
  }
}