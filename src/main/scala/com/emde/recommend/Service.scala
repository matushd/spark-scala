package com.emde.recommend

import org.apache.spark.rdd.RDD
import scala.util.parsing.json.JSON
import com.emde.model.Like
import com.emde.model.Event
import org.apache.spark.{SparkContext}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorIndexer
import org.apache.spark.ml.regression.{RandomForestRegressionModel, RandomForestRegressor}
import org.apache.spark.sql.SQLContext
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import com.emde.repository.RecommendedRepository

object Service {
  
  def prepareTraineeData(path: String, similar: (Int, List[Int]), movieIdMap: RDD[(Int, Int)],
      views: RDD[(Int, (Double, Int))]) {
    val movieIdFilter = similar._2 :+ similar._1
    //Default similar movie ratings
    val defaults = similar._2.map { x => (x.toDouble, 0.0) }.toMap
    
    val movieIdMapFiltered = movieIdMap.filter(x => movieIdFilter.contains(x._1.toInt))
    
    // ( tmdbid, ( movieid (rating, userId) ) )
    val viewsTmdb = movieIdMapFiltered.join(views)
    
     // ( userId, (tmdbid, rating) )
     val indexViewsTmdb = viewsTmdb.map( x => (x._2._2._2, (x._1.toDouble, x._2._2._1)) )
     
     // ( userId, (tmdbid, rating).. ) posortowane po tmdbid oraz zmergowane z domyślnymi ocenami - 0.0
     // Remove records with only 0 results
     val collect = indexViewsTmdb.groupByKey().map( x => ( (x._2.toList ++ defaults.toList )
         .groupBy( _._1).map { case (k,v) => k -> v.map(_._2).sum }  ).toSeq.sortBy(_._1).toArray )
         .filter(x => x.filter(p => p._2 != 0.0).size > 0)
     
     //Labeled point where label is review of movie is greather than 0 then 1, and properties are similar moives reviews
     val points = collect.map{ x => LabeledPoint(
         if(x.filter(p => p._1 == similar._1)(0)._2 > 0.0) 1 else 0, 
         Vectors.dense(x.filter(p => p._1 != similar._1).map(_._2))) 
         }
     // (tmdbid, ratings)
    
    //movieId, rate, books, music, tv, movies
    MLUtils.saveAsLibSVMFile(points, path)
  }
  
  //http://spark.apache.org/docs/latest/mllib-ensembles.html#training
  def generatePredictionModel(ssc: SparkContext, path: String, numTrees: Int = 16,
      maxDepth: Int = 4, maxBins: Int = 100, trainingCount: Double = 0.7) {
    // Load and parse the data file.
    val data = MLUtils.loadLibSVMFile(ssc, path)

    // Prepare training and test data
    val splits = data.randomSplit(Array(trainingCount, 1 - trainingCount))
    val (trainingData, testData) = (splits(0), splits(1))
    
    // Classifier parameters
    val numClasses = 2
    val categoricalFeaturesInfo = Map[Int, Int]()
    val featureSubsetStrategy = "auto"
    val impurity = "gini"
    
    val model = RandomForest.trainClassifier(trainingData, numClasses,
        categoricalFeaturesInfo, numTrees, featureSubsetStrategy,
        impurity, maxDepth, maxBins)
    
    // Test model and compute test error
    val labelAndPreds = testData.map { point =>
      val prediction = model.predict(point.features)
      (point.label, prediction)
    }
    
    val testErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testData.count()
    println("Random forests model:\n" + model.toDebugString)
    println("Random forests parameters: " + Array(numTrees, maxDepth, maxBins, trainingCount, 1 - trainingCount))
    println("Error: " + testErr)
    
    // Save and load model
    model.save(ssc, path.concat("/predictionModel"))
    val sameModel = RandomForestModel.load(ssc, path.concat("/predictionModel"))
  }
  
  def prepareRating (lines: RDD[String], ssc: SparkContext) : RDD[(BigInt, (Double, Double))] = {
    //Prepare event logs
    val split = lines.flatMap(line => {
      line.split(" ~ ")
    })
    val events = split.map({ x => JSON.parseFull(x) }).filter({ _.isDefined })
    .map { x => new Event(x.get.asInstanceOf[Map[String, Any]]) }.filter { x => x.userId.toString().length() > 0 && x.path.startsWith("/movie/") }
    // ( userId, movieId )
    val userEvents = events.map { x => (BigInt(x.userId.toString), (x.path split ("/movie/"))(1) ) }
      .filter(x => x._2 forall Character.isDigit)
    // ( userId, ( movieId, views ) )
    userEvents.map(x => (x, 1)).reduceByKey((a, b) =>  a + b).map(x => (x._1._1, (x._1._2.toDouble, x._2.toDouble)))
  }
  
  def predictRecommend(path: String, ratings: RDD[(BigInt, (Double, Double))], similar: (Int, List[Int]),
      ssc: SparkContext) {
    val movieIdFilter = similar._2 :+ similar._1
    
    //Default similar movie ratings
    val defaults = similar._2.map { x => (x.toDouble, 0.0) }.toMap
    
    // ( userId, ( movieId, views ) )
    val viewsFiltered = ratings.filter(x => movieIdFilter.contains(x._2._1.toInt))
    
    // ( userId, (tmdbid, rating).. ) posortowane po tmdbid oraz zmergowane z domyślnymi ocenami - 0.0
     val collect = viewsFiltered.groupByKey().map( x => ( x._1, (x._2.toList ++ defaults.toList ).groupBy( _._1)
         .map { case (k,v) => k -> v.map(_._2).sum }.toSeq.sortBy(_._1).toArray  ) )
         .filter(x => x._2.filter(p => p._2 != 0.0).size > 0)
    
    // Points to prediction
    val points = collect.map { x => LabeledPoint(
        x._1.toDouble,
        Vectors.dense(x._2.filter(p => p._1 != similar._1).map(_._2)))
        }
    
    // Load model
    val sameModel = RandomForestModel.load(ssc, path.concat("/predictionModel"))
    
    // Evaluate model on test instances and compute test error
    val labelAndPreds = points.map { point =>
      val prediction = sameModel.predict(point.features)
      (point.label, prediction)
    }
    
    // Insert to db
    labelAndPreds.foreach(x => {
        println(BigDecimal(x._1).toBigInt.toString(), similar._1, x._2)
        RecommendedRepository.create(BigDecimal(x._1).toBigInt.toString(), similar._1, x._2)
    })
  }
}