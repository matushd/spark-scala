package com.emde.model

class Like(json: Map[String, Any]) {
  val sessionId = json("sessionId")
  val userId = json("userId")
  val books = json("books").asInstanceOf[List[String]]
  val movies = json("movies").asInstanceOf[List[String]]
  val tv = json("tv").asInstanceOf[List[String]]
  val music = json("music").asInstanceOf[List[String]]
  override def toString(): String = "(books: " + books + ", movies: " + movies + ", tv: " + tv +
  ", music: " + music + ", sessionId: " + sessionId + ", userId:" + userId + ")";
}