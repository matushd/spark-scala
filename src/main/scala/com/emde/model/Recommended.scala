package com.emde.model

import org.joda.time._
import scalikejdbc._

case class Recommended(id: Integer, userId: String, movieId: Integer, prediction: Double, date: DateTime)

object Recommended extends SQLSyntaxSupport[Recommended] {
  
  override val tableName = "recommended"
  
  override val columns = Seq("recommended_id", "recommended_userid", "recommended_movie_id", "recommended_prediction", "recommended_date")
  
  override val nameConverters = Map(
      "^id$" -> "recommended_id",
      "^userId$" -> "recommended_userid",
      "^movieId$" -> "recommended_movie_id",
      "^prediction$" -> "recommended_prediction",
      "^date$" -> "recommended_date"
      )
  
  def apply(rs: WrappedResultSet) = new Recommended(rs.int("recommended_id"), rs.string("recommended_userid"),
      rs.int("recommended_movie_id"), rs.double("recommended_prediction"), rs.jodaDateTime("recommended_date"))
}